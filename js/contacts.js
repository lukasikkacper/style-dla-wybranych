var app = new Vue({
    el: '#app',
    data: {
        persons: [],
        edit: null,
        name: 'Kacper',
        subName: 'Łukasik',
        email: 'test@wp.pl',
        age: 5,
        date: '2020-02-01',
        sex: 'm',
        group: 'Przyjaciele',
        tel: "800900100",
        listKey: 0,
        showPrivate: true,
        errors: {
            name: {state: false, message: ""},
            subName: {state: false, message: ""},
            email: {state: false, message: ""},
            age: {state: false, message: ""},
            date: {state: false, message: ""},
            tel: {state: false, message: ""},
        }
    },
    methods: {
        add: function (e) {
            e.preventDefault();
            let ins = this;

            if (!ins.checkForm()) {
                Swal.fire(
                    'Błąd!',
                    'Nie wszystkie pola zostały wypełnione !',
                    'error'
                );
                return;
            }

            var item = {
                name: ins.name,
                subName: ins.subName,
                email: ins.email,
                age: ins.age,
                date: ins.date,
                sex: ins.sex,
                group: ins.group,
                tel: ins.tel,
            };

            if (this.edit == null) {
                ins.persons.push(item);
                Swal.fire(
                    'Dodano!',
                    'Pomyślnie dodano kontakt!',
                    'success'
                );
            } else {
                ins.persons[ins.edit] = item;
                Swal.fire(
                    'Edytowano!',
                    'Pomyślnie edytowano kontakt!',
                    'success'
                );
            }

            ins.clearForm();
            this.listKey++;

            $('.card-body').collapse("hide");
        },

        checkForm: function () {
            let ins = this;
            let valid = true;

            if (this.name == "") {
                ins.errors.name.state = true;
                valid = false;
                ins.errors.name.message = "Te pole nie może być puste ! ";
            } else ins.errors.name.state = false;

            if (this.subName == "") {
                ins.errors.subName.state = true;
                valid = false;
                ins.errors.subName.message = "Te pole nie może być puste ! ";
            } else ins.errors.subName.state = false;

            if (this.email == "") {
                ins.errors.email.state = true;
                valid = false;
                ins.errors.email.message = "Te pole nie może być puste ! ";
            } else ins.errors.email.state = false;

            if (this.date == "") {
                ins.errors.date.state = true;
                valid = false;
                ins.errors.date.message = "Te pole nie może być puste ! ";
            } else ins.errors.date.state = false;

            if (this.age == "") {
                ins.errors.age.state = true;
                valid = false;
                ins.errors.age.message = "Te pole nie może być puste !";
            } else ins.errors.age.state = false;

            if (this.tel == "") {
                ins.errors.tel.state = true;
                valid = false;
                ins.errors.tel.message = "Te pole nie może być puste !";
            } else ins.errors.tel.state = false;


            return valid;
        },
        clearForm: function () {
            this.edit = null;
            this.name = '';
            this.subName = '';
            this.email = '';
            this.age = 0;
            this.date = '';
            this.sex = 'm';
            this.group = 'Rodzina';
            this.tel = "";
        },
        hasRecords: function () {
            return this.persons.length != 0;
        },
        removeRecord: function (index) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Czy jesteś pewien?',
                text: "Gdy usunieś kontakt, nie bedziesz mógł go przywrócić!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Tak',
                cancelButtonText: 'Nie',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    swalWithBootstrapButtons.fire(
                        'Usunięto!',
                        'Twój kontakt został usunięty.',
                        'success'
                    );
                    this.persons.splice(index, 1);
                }
            })
        },
        cancelEdit: function () {
            this.clearForm();
            $('.card-body').collapse("hide");
        },
        editRecord: function (index) {
            var item = this.persons[index];

            this.edit = index;
            this.name = item.name;
            this.subName = item.subName;
            this.email = item.email;
            this.age = item.age;
            this.date = item.date;
            this.sex = item.sex;
            this.group = item.group;
            this.tel = item.tel;
            $('.card-body').collapse("show");
        },

    },
    filters: {
        lowercase: function (v) {
            return v.toLowerCase();
        },
        fullsex: function (v) {
            return v == 'm' ? "Meżczyzna" : "Kobieta";
        },
        telephoneFormat: function (v) {
            if (v.length == 9) {
                let temp = v.match(/.{1,3}/g);
                return temp[0] + "-" + temp[1] + "-" + temp[2];
            } else {
                return "Nie poprawny format telefonu";
            }
        },
    },
    mounted: function () {
        $('.card-body').collapse("toggle");
    }
});

